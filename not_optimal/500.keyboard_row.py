class Solution(object):
    def findWords(self, words):
        """
        :type words: List[str]
        :rtype: List[str]
        """
        first, second, third = set("qwertyuiop"), set("asdfghjkl"), set("zxcvbnm")
        result = []
        for word in words:
            s = set(word.lower())
            if s.issubset(first) or s.issubset(second) or s.issubset(third):
                result.append(word)
        return result

    def findWords2(self, words):
        """
        :type words: List[str]
        :rtype: List[str]
        """
        first, second, third = set("qwertyuiop"), set("asdfghjkl"), set("zxcvbnm")
        result = []
        for word in words:
            s = set(word.lower())
            if (s - first) == set() or (s - second) == set() or (s - third) == set():
                result.append(word)
        return result


solution = Solution()
tests = [
    [["ask", "Adf", "qwe", "qse", "qzert"], ["ask", "Adf", "qwe"]],
    [["a", "x", "xx", "xd", "q"], ["a", "x", "xx", "q"]],
    [["Zal", "Zxc", "Qwe"], ["Zxc", "Qwe"]]
]
for i, exp in tests:
    res = solution.findWords2(i)
    if res != exp:
        print "result is " + str(res) + "but exp is " + str(exp)
    else:
        print "OK"
